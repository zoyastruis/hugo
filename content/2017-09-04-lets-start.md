---
title: Let's start!
subtitle: 4 september 2017
date:2017-09-04
tags:["blog","blogging,"nieuw","page"]
---


4 september 2017, de eerste officiële lesweek op de 
 Hoge School Rotterdam. Een frisse start, ik heb er zin in!

Het was fijn dat toen we maandag begonnen, de opdracht al vrij duidelijk was. 
De briefing werd nog een keer doorgenomen en de kern van de opdracht was:
*Ontwerp een spel dat gebruik maakt van een online interactief element dat in 
de introductieweek van 2018 studenten in Rotterdam met elkaar en met de stad 
Rotterdam verbindt.*

Hoe pak je dit aan met 5 mensen die elkaar pas een week kennen? 
Allemaal andere ideeën, iedereen wilt iets inbrengen. Ik merkte dat we 
automatisch gingen brainstormen maar niet over de "goede dingen". 
We waren al te veel bezig met het bedenken van een spel. 

Toen we besefte dat dit geen goed begin was heb ik het voortouw genomen. 
Dit heb ik gedaan omdat ik van mijzelf weet dat ik heel erg gestructureerd 
ben en ik dacht dat wij als groep op dat moment structuur nodig hadden.

Hoe heb ik dit aangepakt? Natuurlijk wist ik ook niet precies wat wij moesten 
doen, maar het leek mij duidelijk dat er onderzoek gedaan moest worden.
(dit stond ook in de briefing omschreven). De logische volgende stap is een 
taakverdeling maken zodat voor iedereen helder is wie wat gaat doen. 

Iedereen staat op het punt om aan de slag te gaan. 
Tot de vraag opeens gesteld word : 
*"Eigenlijk kunnen we niks onderzoeken zonder doelgroep"* . 
Gezamelijk hebben wij gekeken welke doelgroep ons het meest zou aanspreken. 
Een creatieve opleiding!.. Maar niet CMD, dat is te veilig. 
Op deze manier kwamen we uit op de opleiding Arts & Crafts op de WDKA. 

Eindelijk! We konden beginnen. Van het een kwam het ander en we besloten 
dat we een enquête of interview moesten houden om er echt achter te komen 
wat onze doelgroep wil. Zo gestructureerd als dat ik ben :) 
maakte ik een nieuwe plan van aanpak. 
*Wanneer gaan we deze enquête afnemen?* 
Dit wouden we zo snel mogelijk doen, we hadden besloten dat we maandag zouden 
besteden aan vragen maken en dinsdag 5-09 uit school naar de WDKA zouden gaan.
*Wat gaan we NU doen?*
Goede vragen bedenken, evt een enquête maken.

We hadden tussen door gesprekken met vakdocenten. En legden uit wat we wouden 
doen. De ene docent vertelde dat het, het best was om een interview te houden. 
De andere docent zei dat een enquête ook goed kon. Lastig dus..

We besloten een combinatie te doen. Rond de 15 personen korte vragen te 
stellen en erna een korte enquête in de laten vullen. 

Ik nam samen met Emma de taak om enquête te maken op ons. Ik zocht een goede 
website waar ook duidelijke resultaten uit zouden komen. Wij kwamen uit op 
*Google Formulieren*.

Een eerste studiodag waarin we veel gedaan hebben met zijn alle.

5 september 2017
**WDKA enquête** 
Na school liepen we gelijk met ons groepje naar de WDKA.Ik vond het heel 
spannend om mensen aan te spreken of ze ons wouden helpen. Het hielp wel 
dat onze vakdocent tegen ons zei, als het ondersom was zou je het erg vinden 
om aangesproken te worden? nee, totaal niet dacht ik. Zo stonden wij op de WDKA 
ik merkte dat mijn mede team genoten het ook spannend vonden, daarom begon ik 
iemand aan te spreken en van het een kwam het ander. En hadden wij onze vragen 
gesteld.